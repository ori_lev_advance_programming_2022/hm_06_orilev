#include <iostream>


#define ERROR_MESSAGE "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year"
#define SUCCESS 0
#define ERROR 1

#define INVALID_RESULT 8200


int add(int a, int b, int& error_flag) {

    error_flag = SUCCESS;

    // check if the result is invalid
    if (a + b == INVALID_RESULT)
    {
        std::cerr << ERROR_MESSAGE << std::endl;
        error_flag = ERROR;
    }

    return a + b;
}


int  multiply(int a, int b, int& error_flag) {
    int sum = 0;
    // create a new varible to check weather the add function return error
    int invalid_add_calculation = 0;
    
    // put in error flag success, because the current code did not have an error
    error_flag = SUCCESS;

    for(int i = 0; i < b; i++) {
        sum = add(sum, a, invalid_add_calculation);
        
        // check if add function or local varible do something wrong
        if ((invalid_add_calculation == ERROR) || (sum == INVALID_RESULT) || (i == INVALID_RESULT))
        {
            error_flag = ERROR;
            std::cerr << ERROR_MESSAGE << std::endl;
        }
    }
    return sum;
}

int  pow(int a, int b, int& error_flag) {
    int exponent = 1;

    // create a new varible to check weather the add function return error
    int invalid_add_calculation = SUCCESS;


    for(int i = 0; i < b; i++) {
        exponent = multiply(exponent, a, invalid_add_calculation);
        // check if add function or local varible do something wrong
        if ((invalid_add_calculation == ERROR) || (exponent == INVALID_RESULT) || (i == INVALID_RESULT))
        {
            error_flag = ERROR;
            std::cerr << ERROR_MESSAGE << std::endl;
        }
    }
    return exponent;
}

int main(void) {
    int a = 0;
    int b = 0;
    int flag;
    int sum = 0;

    std::cout << "Enter first num to add: ";
    std::cin >> a;
    std::cout << "Enter second num to add: ";
    std::cin >> b;

    sum = add(a, b, flag);
    if (flag == SUCCESS)
        std::cout << "The result is: " << sum << std::endl << std::endl;
    else
        std::cout << "Error!" << std::endl << std::endl;
    

    std::cout << "Enter first num to mul: ";
    std::cin >> a;
    std::cout << "Enter second num to mul: ";
    std::cin >> b;

    sum = multiply(a, b, flag);
    if (flag == SUCCESS)
        std::cout << "The result is: " << sum << std::endl << std::endl;
    else
        std::cout << "Error!" << std::endl << std::endl;


    std::cout << "Enter first num to pow: ";
    std::cin >> a;
    std::cout << "Enter second num to apow: ";
    std::cin >> b;

    sum = pow(a, b, flag);
    if (flag == SUCCESS)
        std::cout << "The result is: " << sum << std::endl << std::endl;
    else
        std::cout << "Error!" << std::endl << std::endl;
}

