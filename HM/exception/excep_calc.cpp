#include <iostream>
#include <string>

#define ERROR_MESSAGE "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year"

#define INVALID_RESULT 8200

int add(int a, int b) {
    if (a + b == INVALID_RESULT)
    {
        throw((std::string)ERROR_MESSAGE);
    }
    return a + b;
}

int  multiply(int a, int b) {
    int sum = 0;
    for(int i = 0; i < b; i++) {
        sum = add(sum, a);
        if (sum == INVALID_RESULT || i == INVALID_RESULT)
            throw(ERROR_MESSAGE);
    }
    return sum;
}

int  pow(int a, int b) {
    int exponent = 1;
    for(int i = 0; i < b; i++) {
        exponent = multiply(exponent, a);
        if (exponent == INVALID_RESULT || i == INVALID_RESULT)
            throw(ERROR_MESSAGE);
    }
    return exponent;
}


int main()
{
    int a, b, result;
    try
    {
        std::cout << "Enter first num to add: ";
        std::cin >> a;
        std::cout << "Enter second num to add: ";
        std::cin >> b;

        result = add(a, b);
        std::cout << "The result is: " << result << std::endl << std::endl;
    }
    catch (const std::string& e)
    {
        std::cerr << "ERROR: " << e << std::endl << std::endl;
    }

    try
    {
        std::cout << "Enter first num to mul: ";
        std::cin >> a;
        std::cout << "Enter second num to mul: ";
        std::cin >> b;

        result = multiply(a, b);
        std::cout << "The result is: " << result << std::endl << std::endl;
    }
    catch (const std::string& e)
    {
        std::cerr << "ERROR: " << e << std::endl << std::endl;
    }


    try
    {
        std::cout << "Enter first num to pow: ";
        std::cin >> a;
        std::cout << "Enter second num to pow: ";
        std::cin >> b;

        result = pow(a, b);
        std::cout << "The result is: " << result << std::endl << std::endl;
    }
    catch (const std::string& e)
    {
        std::cerr << "ERROR: " << e << std::endl << std::endl;
    }
    return 0;
}
