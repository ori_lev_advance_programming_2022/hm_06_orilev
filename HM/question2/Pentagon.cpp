#include "Pentagon.h"

#include "shape.h"

#include "shapeexception.h"
#include <iostream>
#include "mathUnils.h"

Pentagon::Pentagon(std::string& name, std::string& color, double rib_length) :
	Shape(name, color)
{
	this->set_rib(rib_length);
}

Pentagon::~Pentagon()
{
}

void Pentagon::draw()
{
	std::cout << "name: " << this->getName() << std::endl;
	std::cout << "color: " << this->getColor() << std::endl;
	std::cout << "rib: " << this->get_rib() << std::endl;
	std::cout << "area: " << MathUnils::CalPentagonArea(this->get_rib()) << std::endl << std::endl;
}

void Pentagon::set_rib(double rib)
{
	if (rib < 0)
	{
		throw shapeException();
	}
	_rib = rib;
}

double Pentagon::get_rib() const
{
	return this->_rib;
}
