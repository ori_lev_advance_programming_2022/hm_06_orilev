#pragma once

#ifndef HEXAGON_H

#define HEXAGON_H

#include "shape.h"
#include <string>

class Hexagon : public Shape {
public:
	Hexagon(std::string& name, std::string& color, double rib_length);
	~Hexagon();
	void draw();
	void set_rib(double rib);
	double get_rib() const;

private:
	double _rib;
};


#endif // !HEXAGON_H