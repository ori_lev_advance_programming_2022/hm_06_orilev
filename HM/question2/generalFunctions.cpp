#include "generalFunctions.h"
#include "InputException.h"

#include <iostream>

void throw_input_exception()
{
	if (std::cin.fail())
	{
		throw InputException();
	}
}