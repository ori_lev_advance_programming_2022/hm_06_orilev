#include "mathUnils.h"

#include <math.h>

double MathUnils::CalPentagonArea(double rib_length)
{
    return (pow(rib_length, 2) / 4) * (sqrt((25 + (10 * sqrt(5)))));
}

double MathUnils::CalHexagonArea(double rib_length)
{
    return 2.59808 * pow(rib_length, 2);
}
