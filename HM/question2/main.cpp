#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include "InputException.h"

#include "Pentagon.h"
#include "Hexagon.h"


#define EMPTY_STRING 0
#define GOOD_INPUT_LENGTH 1

int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180; int height = 0, width = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);

	Pentagon pent(nam, col, rad);
	Hexagon hexa(nam, col, rad);

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;

	Shape *ptrheax = &hexa;
	Shape *ptrpent = &pent;

	std::string check_user_input;
	
	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p'; char shapetype;
	char x = 'y';
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, h=hexagon, n=pentagon" << std::endl;


		std::getline(std::cin, check_user_input);
		if (check_user_input.length() == EMPTY_STRING)
		{
			std::cerr << "ERROR: empty input!" << std::endl;
			_exit(1);
		}
		else {
			shapetype = check_user_input[0];
			
			if (check_user_input.length() != GOOD_INPUT_LENGTH)
			{
				std::
					cerr << "Warning � Don�t try to build more than one shape at once" << std::endl;
			}
		}
try
		{

			switch (shapetype) {
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam;

				getchar(); // clear the buffer
				std::cin >> rad;

				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col;
				getchar(); // clear the buffer
				std::cin >> height >> width;
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col;
				getchar(); // clear the buffer
				std::cin >> height >> width;
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col;
				getchar(); // clear the buffer
				std::cin >> height >> width >> ang >> ang2;
				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
			
			case 'h':
				std::cout << "enter name, color, rib" << std::endl;
				std::cin >> nam >> col;
				getchar(); // clear the buffer
				std::cin >> rad;
				hexa.setName(nam);
				hexa.setColor(col);
				hexa.set_rib(rad);
				ptrheax->draw();
				break;
			case 'n':
				std::cout << "enter name, color, rib" << std::endl;
				std::cin >> nam >> col;
				getchar(); // clear the buffer
				std::cin >> rad;
				pent.setName(nam);
				pent.setColor(col);
				pent.set_rib(rad);
				ptrpent->draw();
				break;
			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;

			getchar(); // clear the buffer

			x = getchar();
		}
		catch (InputException& e)
		{
			printf(e.what());
			std::cout << std::endl;
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
		catch (std::exception& e)
		{			
			printf(e.what());
			std::cout << std::endl;
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}

		getchar(); // clear the buffer
	}



		system("pause");
		return 0;
	
}