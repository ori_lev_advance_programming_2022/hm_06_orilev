#pragma once

#ifndef PENTAGON_H
#define PENTAGON_H
#include "shape.h"
#include <string>

class Pentagon : public Shape {
public:
	Pentagon(std::string& name, std::string& color, double rib_length);
	~Pentagon();
	void draw();
	void set_rib(double rib);
	double get_rib() const;

private:
	double _rib;
};
#endif // !PENTAGON_H
