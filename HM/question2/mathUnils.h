#pragma once
#ifndef MATHUNILS_H

#define MATHUNILS_H

class MathUnils
{
public:
	static double CalPentagonArea(double rib_length);
	static double CalHexagonArea(double rib_length);
};

#endif // !MATHUNILS_H
